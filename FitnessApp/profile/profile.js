import React, { useState } from 'react';
import { ImagePicker, Permissions } from 'expo';
import { View, Text, Image, StyleSheet } from 'react-native';
import { Avatar, Card, Title, Paragraph, Button } from 'react-native-paper';

function ProfileScreen( data ) {
  
  data = JSON.parse(JSON.stringify(data));

  data = data["route"]["params"]["dataUser"]["dataUser"];
  
  return (
    <View style={styles.container}>
      <Avatar.Image size={150} source={{uri: data["profilna"]}} />
      <Card style={styles.card}>
        <Card.Content>
          <Title>{data["name"]} {data["surname"]}</Title>
          <Paragraph>Starost: {data["age"]}</Paragraph>
          <Paragraph>Spol: {data["gender"]}</Paragraph>
          <Paragraph>Teža: {data["weight"]} kg</Paragraph>
          <Paragraph>Višina: {data["height"]} cm</Paragraph>
        </Card.Content>
      </Card>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  card: {
    marginTop: 20,
    width: '100%',
  },
});

export default ProfileScreen;
