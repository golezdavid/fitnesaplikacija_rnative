import * as React from 'react';
import { Text, View, StyleSheet, Button, Image, TouchableOpacity, Alert } from 'react-native';
import { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import StopClock from './stopwatch/Stopclock';
import StevecKalorij from './steveckalorij/Steveckalorij';
import FitnessPlan from './fitnesPlan/fitnesPlan';

import { TextInput } from "react-native-gesture-handler";

import { initializeApp } from 'firebase/app';
import { getDatabase, ref, set, onValue } from "firebase/database";
import BMI from './bmiCalculator/bmiUI';
import ProfileScreen from './profile/profile';
import Jedilniki from './jedilniki/jedeilnik'

export default function App() {

  const firebaseConfig = {
    apiKey: "AIzaSyBrmnF9Q5DRGRvaVqIhRFz2Jq2vlde7Ztc",
    authDomain: "fitness-app-f1c17.firebaseapp.com",
    databaseURL: "https://fitness-app-f1c17-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "fitness-app-f1c17",
    storageBucket: "fitness-app-f1c17.appspot.com",
    messagingSenderId: "886175124473",
    appId: "1:886175124473:web:698830c05a897d46d3cb28",
    measurementId: "G-THFZKV346D",
    databaseURL: "https://fitness-app-f1c17-default-rtdb.europe-west1.firebasedatabase.app/"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);

  const database = getDatabase(app);

  const [loggedIn, setLoggedIn] = React.useState(false)
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [dataUser, setDataUser] = React.useState();
 

  function checkUser() {

    const login = ref(database, 'users/' + username);

    onValue(login, (snapshot) => {
      const data = snapshot.val();

      if (password == data.pass) {
        setDataUser(data);
        setLoggedIn(true);
      } else {
        Alert.alert('Prijava neuspešna!', 'Napačno uporabniško ime ali geslo', [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
        setLoggedIn(false);
      }
    });
  }

  return (<>
    {loggedIn ? (
      <NavigationContainer>
        <MyTabs  dataUser={dataUser} />
      </NavigationContainer>
    ) : (
      <View style={styles.containerLogin}>
        <Image source={require('./assets/fitnesLogo.jpg')} style={styles.image} />
        <TextInput
          style={styles.input}
          placeholder="Username"
          onChangeText={novo => setUsername(novo)}
        />
        <TextInput
          style={styles.input}
          placeholder="Password"
          secureTextEntry={true}
          onChangeText={novo => setPassword(novo)}
        />
        <TouchableOpacity style={styles.button} onPress={checkUser}>
          <Text style={styles.buttonText}>Prijava</Text>
        </TouchableOpacity>
      </View>
    )}
  </>
  )
}

const Tab = createBottomTabNavigator();

function MyTabs(dataUser) {


  const firebaseConfig = {
    apiKey: "AIzaSyBrmnF9Q5DRGRvaVqIhRFz2Jq2vlde7Ztc",
    authDomain: "fitness-app-f1c17.firebaseapp.com",
    databaseURL: "https://fitness-app-f1c17-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "fitness-app-f1c17",
    storageBucket: "fitness-app-f1c17.appspot.com",
    messagingSenderId: "886175124473",
    appId: "1:886175124473:web:698830c05a897d46d3cb28",
    measurementId: "G-THFZKV346D",
    databaseURL: "https://fitness-app-f1c17-default-rtdb.europe-west1.firebasedatabase.app/"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);


  return (
    <Tab.Navigator
      initialRouteName="Profil"
      screenOptions={{
        tabBarActiveTintColor: '#009688',
      }}
    >
      <Tab.Screen
        name="BMI"
        component={BMI}
        options={{
          tabBarLabel: 'BMI',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="calculator" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Fitnes plani"
        component={FitnessPlan}
        options={{
          tabBarLabel: 'Fitnes plani',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="bell" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Jedilniki"
        component={Jedilniki}
        options={{
          tabBarLabel: 'Jedilniki',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="food" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Števec Kalorij"
        component={StevecKalorij}
        options={{
          tabBarLabel: 'StevecKalorij',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="fire" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Profil"
        component={ProfileScreen}
        initialParams={{dataUser}}
        options={{
          tabBarLabel: 'Profil',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
/*
export default function App() {


  return (
    <NavigationContainer>
      <MyTabs />
    </NavigationContainer>
  );
}*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 20,
  },
  header: {
    fontSize: 24,
    marginBottom: 20,
  },
  form: {
    width: '100%',
  },
  seznam: {
    textTransform: 'uppercase',
  },
  label: {
    marginBottom: 10,
  },
  picker: {
    width: '100%',
  },
  input: {
    width: '100%',
    borderWidth: 1,
    borderColor: 'gray',
    padding: 5,
    marginBottom: 10,
  },
  total: {
    fontSize: 20,
    marginBottom: 20,
  },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#009688",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  },
  containerLogin: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',

  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  image: {
    width: 500,
    height: 250,
    resizeMode: 'contain',
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    margin: 10,
    padding: 10,
    width: '90%',
    borderRadius: 5
  },
  button: {
    backgroundColor: '#009688',
    padding: 10,
    margin: 10,
    width: '90%',
    borderRadius: 5
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
  }
});