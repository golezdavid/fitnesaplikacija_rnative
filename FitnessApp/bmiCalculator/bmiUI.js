import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, View, KeyboardAvoidingView, TouchableOpacity, Platform } from 'react-native';
import { TextInputMask } from 'react-native-masked-text';
import Constants from 'expo-constants';

export default function BMI() {
  const [masa, setMasa] = useState(0)
  const [visina, setVisina] = useState('')
  const [result, setResult] = useState(0)
  const [resultText, setResultText] = useState('')

  function calcular() {
    const result = masa / (Math.pow(visina, 2))
    setResult(result)

    if (result < 17) {
      setResultText('Podhranjenost')
    } else if (result < 18.5) {
      setResultText('Rahla podhranjenost')
    } else if (result < 25) {
      setResultText('Normalno')
    } else if (result < 30) {
      setResultText('Rahla debelost')
    } else if (result < 35) {
      setResultText('Debelost')
    } else if (result < 40) {
      setResultText('Previlika debelost')
    } else if (result >= 40) {
      setResultText('Začnite hujšati')
    } else {
      setResultText('Vnesite podatke!')
      setResult(0)
    }
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <View style={styles.inputs} >
        <TextInput
          placeholder="Masa"
          placeholderTextColor={'gray'}
          keyboardType="numeric"
          returnKeyType="done"
          style={styles.input}
          onChangeText={(massa) => setMasa(massa)}
        />
        <TextInputMask
          placeholder="Višina"
          placeholderTextColor={'gray'}
          keyboardType="numeric"
          returnKeyType="done"
          style={styles.input}
          type={'money'}
          options={{
            precision: 2,
            separator: '.',
            delimiter: '.',
            unit: '',
            suffixUnit: ''
          }}
          value={visina}
          onChangeText={(altura) => {setVisina(altura)}}
        />
      </View>
      <TouchableOpacity onPress={calcular} style={styles.button} activeOpacity={0.6}>
        <Text style={styles.buttonText}>Izračunaj BMI</Text>
      </TouchableOpacity>
      <Text style={styles.result}>{result.toFixed(2)}</Text>
      <Text style={[styles.result, { fontSize: 35 }]}>{resultText}</Text>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#fff',
    paddingTop: Constants.statusBarHeight,
  },
  inputs: {
    flexDirection: 'row',
    marginBottom: 40,
  },
  input: {
    height: 80,
    width: '50%',
    textAlign: 'center',
    fontSize: 50,
    color: 'gray'
  },
  button: {
    backgroundColor: '#009688',
    marginHorizontal: 80,
    borderRadius: 8,
  },
  buttonText: {
    padding: 30,
    alignSelf: 'center',
    fontSize: 25,
    color: 'white',
    fontWeight: 'bold'
  },
  result: {
    alignSelf: 'center',
    color: 'gray',
    fontSize: 65,
    padding: 15,
  },
});