import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet, ScrollView } from 'react-native';
import {Picker} from '@react-native-picker/picker';
import { TouchableOpacity } from 'react-native-gesture-handler';

const fetch = require('node-fetch');

const CalorieCounter = () => {

  const [seznamHrane, setSeznamHrane] = useState([]);
  const [food, setFood] = useState('');
  const [quantity, setQuantity] = useState(0);
  const [calories, setCalories] = useState(0);

  const onFoodChange = (itemValue) => {
    setFood(itemValue);
  };

  const onQuantityChange = (text) => {
    setQuantity(text);
  };

const totalCalories = seznamHrane.reduce((total, item) => {
    return total + (calories * item.quantity / 100);
}, 0);

const roundedTotalCalories = totalCalories.toFixed(4);

  const getCalorieValue = async (hrana) => {

    const url = 'https://nutritionix-api.p.rapidapi.com/v1_1/search/' + hrana + '?fields=nf_calories';

    const options = {
      method: 'GET',
      headers: {
        'X-RapidAPI-Key': '3500045a1amsh57e82689c6f2b60p1f064djsn6b5a6f020d89',
        'X-RapidAPI-Host': 'nutritionix-api.p.rapidapi.com'
      }
    };

    fetch(url, options)
      .then(res => res.json())
      .then(json => setCalories(json['hits'][0]['fields']['nf_calories']) )
      .catch(err => console.error('error:' + err));
  }

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.form}>
        <Text style={styles.label}>Izberite hrano, ki ste jo zaužili:</Text>
        <Picker
          selectedValue={food}
          style={styles.picker}
          onValueChange={onFoodChange}
        >
          <Picker.Item label="Jabolko" value="apple" />
          <Picker.Item label="Banana" value="banana" />
          <Picker.Item label="Pomaranča" value="orange" />
          <Picker.Item label="Brokoli" value="brocolli" />
          <Picker.Item label="Piščanec" value="chicken" />
          <Picker.Item label="Riba" value="fish" />
          <Picker.Item label="Riž" value="rice" />
          <Picker.Item label="Govedina" value="beef" />
          <Picker.Item label="Svinjina" value="pork" />
          <Picker.Item label="Jajca" value="eggs" />
          <Picker.Item label="Grah" value="peas" />
          <Picker.Item label="Krompir" value="potato" />
          <Picker.Item label="Koruza" value="corn" />
          <Picker.Item label="Testenine" value="pasta" />
          <Picker.Item label="Paradižnik" value="tomato" />
          <Picker.Item label="Arašidi" value="peanuts" />
          <Picker.Item label="Kosmiči" value="cereal" />
          <Picker.Item label="Kruh" value="bread" />
          <Picker.Item label="Paprika" value="paprika" />
          <Picker.Item label="Pica" value="pizza" />
          <Picker.Item label="Sir" value="cheese" />
        </Picker>
        <Text style={styles.label}>Vnesite količino:</Text>
        <TextInput
          style={styles.input}
          keyboardType="numeric"
          onChangeText={onQuantityChange}
          value={quantity}
        />
        <Text style={styles.total}>Skupno kalorij: {roundedTotalCalories} kCal</Text>
        <TouchableOpacity style={styles.appButtonContainer} onPress={() => {
            getCalorieValue(food);
            setSeznamHrane([...seznamHrane, {food: food, quantity: quantity}]);
        }} >
            <Text style = {styles.appButtonText}>DODAJ HRANO</Text>
        </TouchableOpacity>
        <Text>Seznam:</Text>
        {seznamHrane.map((food) => (
            <Text style={styles.seznam} key = {food.food}>{food.food}({food.quantity})</Text>
        ))}
      </View>
      <TouchableOpacity style={styles.appButtonContainer} onPress={() => {
    setSeznamHrane([]);
    setFood('');
    setQuantity(0);
    setCalories(0);
}}>
    <Text style={styles.appButtonPonastavi}>Ponastavi seznam</Text>
</TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 20,
  },
  header: {
    fontSize: 24,
    marginBottom: 20,
  },
  form: {
    width: '100%',
  },
  seznam: {
        textTransform: 'uppercase' ,
  },
  label: {
    marginBottom: 10,
  },
  picker: {
    width: '100%',
  },
  input: {
    width: '100%',
    borderWidth: 1,
    borderColor: 'gray',
    padding: 5,
    marginBottom: 10,
  },
  total: {
    fontSize: 20,
    marginBottom: 20,
  },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#009688",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  },
  appButtonPonastavi: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "bottom",
    textTransform: "uppercase",
    bottom: 0
  }
  });
  
  export default CalorieCounter;