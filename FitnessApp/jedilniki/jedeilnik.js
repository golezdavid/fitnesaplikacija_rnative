import React, { useState } from 'react';
import { StyleSheet, ScrollView, View, Text, Button, FlatList, Image, Touchable } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Card } from '@rneui/themed';
import Carousel from 'react-native-snap-carousel';


const Jedilniki = () => {

  const [jedilnik, setJedilnik] = useState(null);
  const [vrsta, setVrsta] = useState([]);

  const handleSelectPlan = (plan) => {
    setJedilnik(plan);
    switch (plan) {
      case 'zajtrk':
        setVrsta([
          {
            key: '1',
            name: 'Ovseni kosmiči z grškim jogurtom',
            image: 'https://www.mojirecepti.com/slike/domaca-mandljeva-granola-small.jpg',
          },
          {
            key: '2',
            name: 'Sadni smuti',
            image: 'http://www.odprtakuhinja.si/wp-content/uploads/2018/01/Odprtakuhinja-smutiji.jpg',
          },
          {
            key: '3',
            name: 'Jajce in en kos kruha',
            image: 'https://images.24ur.com/media/images/884xX/Sep2019/aaea176453_62304144.jpg?v=d41d',
          },
          {
            key: '4',
            name: 'Omleta',
            image: 'https://images.24ur.com/media/images/953x459/Aug2020/083f8e2920577e950fc8_62451528.jpg?v=f598',
          },
          {
            key: '5',
            name: 'Sendvič',
            image: 'https://www.fratellipizza.rs/wp-content/uploads/2021/10/omlet-frateli-sendvic.jpg',
            
          },
        ]);
        break;
      case 'kosilo':
        setVrsta([
          {
            key: '1',
            name: 'Pečen piščanec in riž',
            image: 'https://www.mojirecepti.com/slike/pomarancni-piscanec.jpg',
          },
          {
            key: '2',
            name: 'Golaž s polento',
            image: 'https://images.24ur.com/media/images/1024x576/Mar2018/ded751a4c9_62051423.jpg?v=d41d',
            
          },
          {
            key: '3',
            name: 'Piščanec v smetanovi omaki',
            image: 'https://images.24ur.com/media/images/575x534/Oct2013/61323909.jpg?v=ba85',
        
          },
          {
            key: '4',
            name: 'Rižota',
            image: 'https://www.kulinarika.net/slikerecepti/18387/1.jpg',

          },
          {
            key: '5',  
            name: 'Taquitosi',
            image: 'https://odprtakuhinja.delo.si/wp-content/uploads/2020/05/Odprtakuhinja-piscancji-taquitosi.jpg',

          }
        ]);
        break;
      case 'vecerja':
        setVrsta([
          {
            key: '1',
            name: 'Špraglji s jajci',
            image: 'https://www.ekodezela.si/wp-content/uploads/2021/03/shutterstock_285101483.jpg',
          },
          {
            key: '2',
            name: 'Poletna ribja solata',
            image: 'https://www.popolnapostava.com/wp-content/uploads/2021/09/Poletna-ribja-solata.jpg',
          },
          {
            key: '3',
            name: 'Bučkina juha',
            image: 'https://www.mojirecepti.com/slike/bucna-juha-5.jpg',
          },
          {
            key: '4',
            name: 'Sushi jar',
            image: 'https://cleanfoodcrush.com/wp-content/uploads/2020/01/Deconstructed-Sushi-Jars-by-CFC.jpg',
          },
          {
            key: '5',
            name: 'Tortelini s šunko',
            image: 'https://images.24ur.com/media/images/1024x576/Sep2021/ea002392bf2d45c4ec17_62615559.jpg?v=040d',
          },
        ]);
        break;
      default:
        setVrsta([]);
        break;
    }
  };

  const handleGoBack = () => {
    setJedilnik(null);
    setVrsta([]);
  };

  return (
    <ScrollView>
      {!jedilnik && (
        <View>
          <Card>
            <Image source={{ uri: 'https://www.bohinj.si/wp-content/uploads/2021/08/bohinjski-zajtrk-2021-10.jpg' }}
              style={{
                width: 300, height: 150, justifyContent: 'center',
                alignItems: 'center', alignSelf: 'center', margin: 10
              }} />
            <TouchableOpacity style={styles.appButtonContainer} onPress={() => handleSelectPlan('zajtrk')} >
              <Text style={styles.appButtonText}>Zajtrk</Text></TouchableOpacity>
          </Card>
          <Card>
            <Image source={{ uri: 'https://odprtakuhinja.delo.si/wp-content/uploads/2020/05/Odprtakuhinja-kosilo-iz-enega-pekaca-Vegeta-Natur.jpg' }}
              style={{
                width: 300, height: 150, justifyContent: 'center',
                alignItems: 'center', alignSelf: 'center', margin: 10
              }} />
            <TouchableOpacity style={styles.appButtonContainer} onPress={() => handleSelectPlan('kosilo')} ><Text style={styles.appButtonText}>Kosilo</Text></TouchableOpacity>
          </Card>
          <Card>
            <Image source={{ uri: 'https://www.vandraj.si/wp-content/uploads/2022/12/bozicna-vecerja-novoletna-vecerja-romanticna-vecerja-recept-kuhanje-kulinarika.jpg' }}
              style={{
                width: 300, height: 150, justifyContent: 'center',
                alignItems: 'center', alignSelf: 'center', margin: 10
              }} />
            <TouchableOpacity style={styles.appButtonContainer} onPress={() => handleSelectPlan('vecerja')} ><Text style={styles.appButtonText}>Večerja</Text></TouchableOpacity>
          </Card>
        </View>
      )}
      {jedilnik && (
        <View>
          <TouchableOpacity style={styles.appButtonContainer} onPress={handleGoBack} ><Text style={styles.appButtonText}>Pojdi nazaj</Text></TouchableOpacity>
          <Carousel style={styles.card}
            data={vrsta}
            renderItem={({ item }) => (
              <Card key={item.key} style={styles.card}>
                <Image source={{ uri: item.image }} style={{ width: 240, height: 200, flex: 1 }} />
                <Text style={styles.velikText}>{item.name}</Text>
              </Card>
            )}
            keyExtractor={(item) => item.key}
            sliderWidth={400}
            itemWidth={300}
          />
        </View>
      )}
    </ScrollView>
  );
};


const styles = StyleSheet.create({
  view: {
    justifyContent: 'center',
    alignItems: 'center',

  },
  card: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center'
  },

  velikText: {
    fontSize: 15,
    textAlign: 'center'

  },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#009688",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    flex: 1,
    textTransform: "uppercase"

  },
});


export default Jedilniki;