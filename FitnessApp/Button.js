import { StyleSheet, View, Pressable, Text } from "react-native";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FontAwesome } from '@expo/vector-icons';

export default function Gumb({ label }) {

    // TODO: -> pri funkciji onPress implementiri avtentikacijo z Google preko Firebase
    return (
        <View style={[styles.buttonContainer, { borderWidth: 4, borderColor: "#ffd33d", borderRadius: 18 }]}>
            <Pressable style={[styles.button, { backgroundColor: "#fff"}]} onPress={()=> alert("You pressed a button!")}>
                <FontAwesome name="google"  size={18} color="black" style={styles.buttonIcon} />
                <Text style={styles.buttonLabel}>{label}</Text>
            </Pressable>
        </View>
    );
}

const styles = StyleSheet.create({
    buttonContainer: {
        width: 200,
        height: 70,
        marginHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 3,
    },
    button: {
        borderRadius: 10,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    buttonIcon: {
        paddingRight: 10,
    },
    buttonLabel: {
        color: 'black',
        fontSize: 16,
    },
});