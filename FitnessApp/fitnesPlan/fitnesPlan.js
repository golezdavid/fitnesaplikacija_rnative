import React, { useState } from 'react';
import { StyleSheet, ScrollView, View, Text, Button, FlatList, Image, Touchable } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Card } from '@rneui/themed';
import Carousel from 'react-native-snap-carousel';
import StopClock from '../stopwatch/Stopclock';


const FitnessPlan = () => {

  //ZA CAROUSEL (SWIPANJE)



  const [selectedPlan, setSelectedPlan] = useState(null);
  const [workouts, setWorkouts] = useState([]);

  const handleSelectPlan = (plan) => {
    setSelectedPlan(plan);
    switch (plan) {
      case 'strength':
        setWorkouts([
          {
            key: '1',
            name: 'Bench Press',
            image: 'https://media.tenor.com/0hoNLcggDG0AAAAC/bench-press.gif',
            reps: '3x8',
            cas: false
          },
          {
            key: '2',
            name: 'Počep',
            image: 'https://media.tenor.com/dIewmQufnJIAAAAC/respect-my-authority-barreras.gif',
            reps: '3x8',
            cas: false
          },
          {
            key: '3',
            name: 'Deadlifts',
            image: 'https://www.journalmenu.com/wp-content/uploads/2018/03/deadlift-gif-side.gif',
            reps: '3x8',
            cas: false
          },
          {
            key: '4',
            name: 'Overhead Press',
            image: 'https://hips.hearstapps.com/ame-prod-menshealth-assets.s3.amazonaws.com/main/assets/how-to-do-the-military-press.gif?resize=480:*',
            reps: '3x8',
            cas: false
          },
        ]);
        break;
      case 'endurance':
        setWorkouts([
          {
            key: '1',
            name: 'Tek',
            image: 'https://media.tenor.com/V5Xzkz8e1VsAAAAM/run-running.gif',
            reps: '5km',
            cas: true
          },
          {
            key: '2',
            name: 'Plavanje',
            image: 'https://i.pinimg.com/originals/1f/37/7a/1f377a713044a0a3ff47410b5fe07561.gif',
            reps: '10 dolzin',
            cas: true
          },
          {
            key: '3',
            name: 'Kolesarjenje',
            image: 'https://media.tenor.com/Qc90hq71WI4AAAAd/cycling-bicycle.gif',
            reps: '20km',
            cas: true

          },
          {
            key: '4',
            name: 'Veslanje',
            image: 'https://cdn.dribbble.com/users/2454049/screenshots/11894321/main-skiff-comp-for-dribble_800sq.gif',
            reps: '1km',
            cas: true

          },
        ]);
        break;
      case 'stabilization':
        setWorkouts([
          {
            key: '1',
            name: 'Spiderman plank',
            image: 'https://flabfix.com/wp-content/uploads/2019/05/Forearm-Spiderman-Plank.gif',
            reps: '1 min',
            cas: true

          },
          {
            key: '2',
            name: 'Stranski plank',
            image: 'https://thumbs.gfycat.com/AgedImprobableIrishwolfhound-size_restricted.gif',
            reps: '30 sek / stran',
            cas: true

          },
          {
            key: '3',
            name: 'Hrbtni plank',
            image: 'http://newlife.com.cy/wp-content/uploads/2019/11/08671301-Reverse-plank_Back_360.gif',
            reps: '1 minuta',
            cas: true

          },
          {
            key: '4',
            name: 'Tapkanje ram',
            image: 'https://flabfix.com/wp-content/uploads/2019/08/Push-Up-with-Shoulder-Taps.gif',
            reps: '3 x 10',
            cas: true

          },
        ]);
        break;
      case 'flexibility':
        setWorkouts([
          {
            key: '1',
            name: 'Cobra raztezanje',
            image: 'https://thumbs.gfycat.com/DarkDentalJackal-max-1mb.gif',
            reps: '3 x 10 sekund',
            cas: true
          },
          {
            key: '2',
            name: 'Mačka - krava razteg',
            image: 'https://www.verywellfit.com/thmb/Jvwv8VPTLwd5gMATDAvuo8ief5M=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/Fitness_Gif-1500x1000-catcow-5c5c85cac9e77c0001566641.gif',
            reps: '3 x 10 sekund',
            cas: true

          },
          {
            key: '3',
            name: 'Raztezanje fleksorjev kolka',
            image: 'https://thumbs.gfycat.com/HeavyScratchyAmericanalligator-max-1mb.gif',
            reps: '3 x 15 sekund',
            cas: true

          },
        ]);
        break;
      default:
        setWorkouts([]);
        break;
    }
  };

  const handleGoBack = () => {
    setSelectedPlan(null);
    setWorkouts([]);
  };

  return (
    <ScrollView>
      {!selectedPlan && (
        <View>
          <Card>
            <Image source={{ uri: 'https://sijem.si/wp-content/uploads/2017/08/vzemi-moc-v-svoje-roke-300x201.jpg' }}
              style={{
                width: 300, height: 150, justifyContent: 'center',
                alignItems: 'center', alignSelf: 'center', margin: 10
              }} />
            <TouchableOpacity style={styles.appButtonContainer} onPress={() => handleSelectPlan('strength')} >
              <Text style={styles.appButtonText}>Moč</Text></TouchableOpacity>
          </Card>
          <Card>
            <Image source={{ uri: 'https://cdn.speedsize.com/4022aca3-422d-45a9-b2f1-0683e17036ac/https://aktivni.metropolitan.si/media/cache/upload/Photo/2015/09/09/plank-deska-na-komolcih-vaja_biggalleryimage.jpg' }}
              style={{
                width: 300, height: 150, justifyContent: 'center',
                alignItems: 'center', alignSelf: 'center', margin: 10
              }} />
            <TouchableOpacity style={styles.appButtonContainer} onPress={() => handleSelectPlan('endurance')} ><Text style={styles.appButtonText}>Vzdržljivost</Text></TouchableOpacity>
          </Card>
          <Card>
            <Image source={{ uri: 'https://www.byrdie.com/thmb/Pm7nS8ynFrLAojChkXmYDe5gbto=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/stretch-fc6eaac3e7fe4fc8bd9801bd02290ee6.jpg' }}
              style={{
                width: 300, height: 150, justifyContent: 'center',
                alignItems: 'center', alignSelf: 'center', margin: 10
              }} />
            <TouchableOpacity style={styles.appButtonContainer} onPress={() => handleSelectPlan('flexibility')} ><Text style={styles.appButtonText}>Raztegljivost</Text></TouchableOpacity>
          </Card>
          <Card>
            <Image source={{ uri: 'https://media.self.com/photos/612d548d5525bd7091ff9e49/2:1/w_2580,c_limit/SE_SEN_07_041_Core%20Day%203%20Stability_Video%20Still%2002.png' }}
              style={{
                width: 300, height: 150, justifyContent: 'center',
                alignItems: 'center', alignSelf: 'center', margin: 10
              }} />
            <TouchableOpacity style={styles.appButtonContainer} onPress={() => handleSelectPlan('stabilization')} ><Text style={styles.appButtonText}>Stabilizacija</Text></TouchableOpacity>
          </Card>
        </View>
      )}
      {selectedPlan && (
        <View>
          <TouchableOpacity style={styles.appButtonContainer} onPress={handleGoBack} ><Text style={styles.appButtonText}>Pojdi nazaj</Text></TouchableOpacity>

          <Carousel
            data={workouts}
            renderItem={({ item }) => (
              <Card key={item.key} >
                <Text style={styles.velikText}>{item.name}</Text>
                <Image source={{ uri: item.image }} style={{ width: 230, height: 200 }} />
                <Text style={styles.velikText}>{item.reps}</Text>
              </Card>
            )}
            keyExtractor={(item) => item.key}
            sliderWidth={400}
            itemWidth={300}
          />
          {selectedPlan == "endurance" ? <StopClock></StopClock>: null || selectedPlan === "stabilization" ?  <StopClock></StopClock>: null ||selectedPlan === "flexibility" ? <StopClock></StopClock> : null}
        </View>
      )}
    </ScrollView>
  );
};


const styles = StyleSheet.create({
  view: {
    justifyContent: 'center',
    alignItems: 'center',

  },

  velikText: {
    fontSize: 35,
    textAlign: 'center'

  },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#009688",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    flex: 1,
    textTransform: "uppercase"

  },
});


export default FitnessPlan;