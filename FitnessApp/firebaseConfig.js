// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBrmnF9Q5DRGRvaVqIhRFz2Jq2vlde7Ztc",
  authDomain: "fitness-app-f1c17.firebaseapp.com",
  databaseURL: "https://fitness-app-f1c17-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "fitness-app-f1c17",
  storageBucket: "fitness-app-f1c17.appspot.com",
  messagingSenderId: "886175124473",
  appId: "1:886175124473:web:698830c05a897d46d3cb28",
  measurementId: "G-THFZKV346D"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);